---

title: "Sidney Persona snippets"

---







## SDR persona snippets by use case

### [Sidney (Systems Administrator)](/handbook/product/personas/#sidney-systems-administrator)

**Overview**
- description

#### [VC&C use case](/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/usecase-gtm/version-control-collaboration/#personas)

##### SDR Discovery Questions

- ...

##### SDR Use Case Snippets

- ...

#### [CI use case](/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/usecase-gtm/ci/#personas)

##### SDR Discovery Questions

- ...

##### SDR Use Case Snippets

- ...

#### [CD use case](/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/usecase-gtm/cd/#personas)

##### SDR Discovery Questions

- ...

##### SDR Use Case Snippets

- ...

#### [DevSecOps use case](/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/usecase-gtm/devsecops/#personas)

##### SDR Discovery Questions

- ...

##### SDR Use Case Snippets

- ...
