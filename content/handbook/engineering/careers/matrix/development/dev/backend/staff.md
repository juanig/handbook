---
title: "Dev Career Framework: Staff Backend Engineer"
---

## Dev Staff Backend Engineer
 
{{% include "includes/engineering/dev-be-career-matrix-nav.md" %}}

**Dev Staff Backend Engineers at GitLab are expected to exhibit the following competencies:**

- [Leadership Competencies](#leadership-competencies)
- [Technical Competencies](#technical-competencies)
- [Values Alignment](#values-alignment)

---

### Leadership Competencies

{{% include "includes/engineering/staff-leadership-competency.md" %}}
{{% include "includes/engineering/development-staff-leadership-competency.md" %}}
- Able to do a deep-dive in the GitLab codebase on any given API endpoint to understand it’s functionality and explain it to others
- Works frequently with other teams to coordinate major changes leading to efficient solutions
- Performs Code Reviews across the GitLab Codebase on a regular cadence


### Technical Competencies

{{% include "includes/engineering/staff-technical-competency.md" %}}
{{% include "includes/engineering/development-staff-technical-competency.md" %}}
- Identifies slow and inefficient code across multiple products
- Vast knowledge of server side programming languages and their frameworks
- Improves engineering standards, tooling, and processes

###  Values Alignment

{{% include "includes/engineering/values-competency.md" %}}
{{% include "includes/engineering/development-staff-values-competency.md" %}}
