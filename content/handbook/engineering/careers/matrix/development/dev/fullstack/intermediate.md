---
title: "Dev Career Framework: Intermediate Fullstack Engineer"
---

## Dev Intermediate Fullstack Engineer
 
{{% include "includes/engineering/dev-fullstack-career-matrix-nav.md" %}}

**Dev Intermediate Fullstack Engineers at GitLab are expected to exhibit the following competencies:**

- [Leadership Competencies](#leadership-competencies)
- [Technical Competencies](#technical-competencies)
- [Values Alignment](#values-alignment)

---

### Leadership Competencies

{{% include "includes/engineering/intermediate-leadership-competency.md" %}}
{{% include "includes/engineering/development-intermediate-leadership-competency.md" %}}

### Technical Competencies

{{% include "includes/engineering/intermediate-technical-competency.md" %}}
{{% include "includes/engineering/development-intermediate-technical-competency.md" %}}

###  Values Alignment

{{% include "includes/engineering/values-competency.md" %}}
{{% include "includes/engineering/development-intermediate-values-competency.md" %}}
